app.controller('LoginController', function ($scope, $rootScope, $state, AuthService) {
    $scope.credentials = {
        username: '',
        password: ''
    };

    $scope.login = function (credentials) {
        AuthService.login(credentials).then(function(){
            $state.go('profile');
        }, function(data){
            $scope.credentials.password = '';
            console.log(data);
        });
    };
})
.controller('ApplicationController', function ($scope, AuthService) {
    $scope.currentUser = null;
    $scope.isAuthenticated = AuthService.isAuthenticated;
    $scope.logout = function(){ AuthService.logout()};
});