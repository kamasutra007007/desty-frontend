// Сервис для хранения токена пользователя
app.service('Storage', function($cookies){
    this.create = function(token){
        $cookies.put('access_token', token);
    };
    this.destroy = function(){
        $cookies.remove('access_token');
    };
    this.get = function(){
        return $cookies.get('access_token');
    };
    return this;
});