app.factory('AuthService', function ($http, Storage, URLS) {
    return {
        login: function (credentials) {
            return $http
                .post(URLS.login, credentials)
                .then(function(response){
                    Storage.create(response.data.token);
                });
        },
        isAuthenticated: function () {
            return !!Storage.get();
        },
        logout: function(){
            return Storage.destroy();
        },
        token: function(){
            return Storage.get();
        }

    };
}).factory('AuthInterceptor', function ($rootScope, $q, AUTH_EVENTS) {
    return {
        responseError: function (response) {
            if (response.status === 401) {
                $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated,
                    response);
            }
            if (response.status === 403) {
                $rootScope.$broadcast(AUTH_EVENTS.notAuthorized,
                    response);
            }
            if (response.status === 419 || response.status === 440) {
                $rootScope.$broadcast(AUTH_EVENTS.sessionTimeout,
                    response);
            }
            return $q.reject(response);
        }
    };
});