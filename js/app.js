var BACKEND_HOST = 'http://127.0.0.1:8000';
var app = angular.module('destyApp', ['ui.router', 'ngCookies'])
    // Ссылка на бекенд

    .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: '/templates/home.html'
            })
            .state('login', {
                url: '^/login',
                templateUrl: '/templates/login.html',
                controller: 'LoginController'
            })
            .state('profile', {
                url: '^/profile',
                templateUrl: '/templates/profile.html'
            })
            .state('404', {
                url: '*',
                templateUrl: 'templates/404.html'
            });
        $urlRouterProvider.otherwise('404');
    }])
    .constant('URLS', {
        'login': BACKEND_HOST + '/login/'
    })
    .constant('STATES', {
        'PUBLIC_STATES': [
            'home',
            'login',
            '404'
        ],
        'PRIVATE_STATES': [
            'profile'
        ]
    })
    .run(function ($rootScope, $state, AuthService, STATES) {
        $rootScope.$on('$stateChangeStart', function (event, next) {
            console.log(next.name);
            console.log(event);
            if (STATES.PRIVATE_STATES.indexOf(next.name) >= 0 && !AuthService.isAuthenticated()){
                $state.go('login');
                event.preventDefault();
            }
            if (next.name == 'login' && AuthService.isAuthenticated()){
                $state.go('home');
                event.preventDefault();
            }
        });
    });