"use strict";

var gulp = require('gulp'),
	concatCSS = require('gulp-concat-css'), // конкатенация
	minifyCSS = require('gulp-minify-css'), // минификация
	livereload = require('gulp-livereload'),// live-сервер
	connect = require('gulp-connect'),		// live-сервер
	plumber = require('gulp-plumber'),      // Вывод ошибок
    ngmin = require('gulp-ngmin');

// Таск для поднятия live-сервера
gulp.task('connect', function() {
  connect.server({
    root: 'app',
    livereload: true
  });
});

gulp.task('html', function(){
	gulp.src('app/index.html')
        .pipe(plumber())
	    .pipe(connect.reload())
});

// Минификация CSS
gulp.task('css', function () {
   return gulp.src('css/*.css')
       .pipe(plumber())
       .pipe(minifyCSS())
       .pipe(gulp.dest('app/css'))
       .pipe(connect.reload())
});

// Минификация Angular
gulp.task('js', function () {
    return gulp.src('js/*.js')
        .pipe(plumber())
        .pipe(ngmin({dynamic: false}))
        .pipe(gulp.dest('app/js'))
        .pipe(connect.reload())
});

// Таск наблюдатель
gulp.task('watch', function(){
	gulp.watch('js/*.js', ['js']);
	gulp.watch('css/*.css', ['css']);
	gulp.watch('app/index.html', ['html']);
});

// Главный таск
gulp.task('default', ['connect', 'html', 'css', 'watch', 'js']);